<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {

            $table->id();
            $table->string('first_name',20)->nullable();
            $table->string('second_name',20)->nullable();
            $table->string('surname',20)->nullable();
            $table->string('second_surname',50)->nullable();
            $table->integer('country_id')->unsigned();
            $table->integer('type_identification_id')->unsigned();
            $table->string('identification',20)->nullable();
            $table->string('mail',300)->unique();
            $table->timestamp('date_start')->nullable();
            $table->integer('work_area_id')->unsigned();
            $table->integer('state_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
