<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(Country::class);
        $this->call(State::class);
        $this->call(TypeIdentification::class);
        $this->call(WorkArea::class);
        $this->call(UserSeeder::class);
    }
}
