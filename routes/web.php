<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::post('/empleados', 'EmpoyeeController');

Auth::routes();

Route::group( ['middleware' => 'auth' ], function(){

    Route::get('/employee', [App\Http\Controllers\EmployeeController::class, 'index'])->name('employee.index');
    Route::get('/home', [App\Http\Controllers\EmployeeController::class, 'create'])->name('employee.create');
    Route::post('/employee/create', [App\Http\Controllers\EmployeeController::class, 'store'])->name('employee.store');
    Route::delete('/employee/delete', [App\Http\Controllers\EmployeeController::class, 'destroy'])->name('employee.delete');
    Route::post('/employee/update', [App\Http\Controllers\EmployeeController::class, 'edit'])->name('employee.edit');
    Route::put('/employee/update/{id}', [App\Http\Controllers\EmployeeController::class, 'update'])->name('employee.update');
});


