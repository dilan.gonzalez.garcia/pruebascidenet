<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Country;

class Employee extends Model
{
    use HasFactory;

    protected $fillable = [ "first_name",  "second_name",  "surname",  "second_surname",  "country_id",  "type_identification_id",  "identification",  "work_area_id", "date_start", ];

    public function country () {
        return $this->belongsTo(Country::class,'country_id','id');
    }

    public function state () {
        return $this->belongsTo(State::class,'state_id','id');
    }

    public function typeidentification () {
        return $this->belongsTo(TypeIdentification::class,'type_identification_id','id');
    }

    public function workarea () {
        return $this->belongsTo(WorkArea::class,'work_area_id','id');
    }
}
