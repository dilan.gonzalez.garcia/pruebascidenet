<?php

namespace App\DataTables;

use App\Models\Employee;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class EmployeeDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', function ( $employee ) {
                return '<button class="btn btn-success btn-edit" onclick="updateEmployee('.$employee->id.')"> <i class="far fa-edit"></i> </button><button class="btn btn-danger btn-delete" onclick="deleteEmployee('.$employee->id.')"> <i class="far fa-trash-alt"></i>    </button>';
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Employee $model
     * @return \Illuminate\Database\Query\Builder
     */
    public function query(Employee $model)
    {
        return $model::with([ 'country',  'state', 'typeidentification', 'workarea' ])->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {

        return $this->builder()
                    ->setTableId('employee-table')
                    ->columns($this->getColumns())
                    /*->columns( [
                        "id" => "ID",
                        "first_name" => "first_name",
                        "second_name" => "second_name",
                        "surname" => "surname",
                        "second_surname" => "second_surname",
                        "country.name" => "country",
                        "state.name" => "state",
                        "typeidentification.name" => "typeidentification",
                        "workarea.name" => "workarea",
                        "created_at" => "created_at",
                        "updated_at" => "updated_at",
                    ] )*/
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1);
                    /*->buttons(
                        Button::make('create'),
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    );*/
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {

        return [
            Column::computed('action')
                  ->exportable(false)
                  ->printable(false)
                  ->width(60)
                  ->addClass('text-center'),
            Column::make('id'),
            Column::make('first_name')->title('Primer nombre'),
            Column::make('second_name')->title("Segundo Nombre"),
            Column::make('surname')->title("Primer Apellido"),
            Column::make('second_surname')->title("Segundo Apellido"),
            Column::make('mail')->title("Correo"),
            Column::make('country.name')->title("País"),
            Column::make('typeidentification.name')->title("Tipo de indentificación"),
            Column::make('identification')->title("Indetificación"),
            Column::make('workarea.name')->title("Area de trabajo"),
            Column::make('date_start')->title("Fecha de ingreso"),
            Column::make('created_at')->title("Fecha Creación"),
            Column::make('updated_at')->title("Actualización"),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Employee_' . date('YmdHis');
    }
}
