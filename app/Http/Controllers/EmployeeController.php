<?php

namespace App\Http\Controllers;

use App\DataTables\EmployeeDataTable;
use App\Models\Country;
use App\Models\Employee;
use App\Models\State;
use App\Models\TypeIdentification;
use App\Models\WorkArea;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class EmployeeController extends Controller
{

    const emailBaseCol = '@cidenet.com.co';
    const emailBaseUsa = '@cidenet.com.us';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @param EmployeeDataTable $dataTable
     * @return mixed
     */
    public function index(EmployeeDataTable $dataTable)
    {
        return $dataTable->render('employee.index',[
            'country' => Country::all(),
            'typeIdentification' => TypeIdentification::all(),
            'workArea' => WorkArea::all(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                "first_name" => "required|max:20|regex:/^([0-9A-Z\s])+$/",
                "second_name" => "max:20|regex:/^[A-Z0-9\s]+$/|nullable",
                "surname" => "required|max:20|regex:/^([0-9A-Z\s])+$/",
                "second_surname" => "max:50|regex:/^([0-9A-Z\s])+$/",
                "country_id" => "required",
                "type_identification_id" => "required",
                "identification" => "required|regex:/^([0-9a-zA-Z])+$/",
                "work_area_id" => "required",
                "date_start" => "required|date"
            ]);
            $validator->setAttributeNames([
                "first_name"=>"Primer nombre",
                "second_name" => "Segundo Nombre",
                "surname" => "Primer Apellido",
                "second_surname" => "Segundo Apellido",
                "country_id" => "País",
                "type_identification_id" => "Tipo de identificación",
                "identification" => "Número de indentificación",
                "work_area_id" => "Area de trabajo",
                "date_start" => "Fecha  ingreso",
            ]);

            if ($validator->fails()) {
                return response()->json(['success' => false, 'errors' => $validator->errors()->all()]);
            }

            $employee = new Employee();
            $employee->fill($request->all());
            $employee->mail = $this->generateEmail( $request->first_name , $request->surname , $request->country_id );
            $employee->state_id = 1;

            if ( $employee->save() )
                return response()->json(['success' => true], 200);
            else
                return response()->json(['success' => false] , 204);

        } catch ( \Exception $exception ) {
            return response()->json(['success' => false , 'error' => $exception->getMessage() ] , 202);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param \App\Models\Employee $employee
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit( Request $request , Employee $employee)
    {
        try {
            $id = $request->get('id');

            $employeeFirst =  $employee::where('id',$id)->first();

            if ( is_null($employeeFirst) ) {
                return response()->json( [ 'success' => false , ] , 200);
            }
            else {
                return response()->json( [ 'success' => true , 'employee' => $employeeFirst ] , 200);
            }

        } catch ( \Exception $e ) {
            return response()->json( [ 'success' => false  ] , 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\JsonResponse
     */
    public function update( Employee $employee , Request $request, $id)
    {
        try {

            $validator = Validator::make($request->all(), [
                "first_name" => "required|max:20|regex:/^([0-9A-Z\s])+$/",
                "second_name" => "max:20|regex:/^[A-Z0-9\s]+$/|nullable",
                "surname" => "required|max:20|regex:/^([0-9A-Z\s])+$/",
                "second_surname" => "max:50|regex:/^([0-9A-Z\s])+$/",
                "country_id" => "required",
                "type_identification_id" => "required",
                "identification" => "required|regex:/^([0-9a-zA-Z])+$/",
                "work_area_id" => "required"
            ]);
            $validator->setAttributeNames([
                "first_name"=>"Primer nombre",
                "second_name" => "Segundo Nombre",
                "surname" => "Primer Apellido",
                "second_surname" => "Segundo Apellido",
                "country_id" => "País",
                "type_identification_id" => "Tipo de identificación",
                "identification" => "Número de indentificación",
                "work_area_id" => "Area de trabajo",
            ]);

            if ($validator->fails())
                return response()->json(['success' => false, 'errors' => $validator->errors()->all()]);

            $employee = Employee::where('id',$id)->first();

            $employee->fill($request->all());
            $employee->mail = $this->generateEmail( $request->first_name , $request->surname , $request->country_id );
            $employee->state_id = 1;

            if ( $employee->update() )
                return response()->json(['success' => true], 200);
            else
                return response()->json(['success' => false] , 200);

        } catch ( \Exception $exception ) {
            return response()->json(['success' => false , 'error' => $exception->getMessage() ] , 202);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param \App\Models\Employee $employee
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request , Employee $employee )
    {
        try {
            $id = $request->get('id');

            $employeeFirst =  $employee::where('id',$id)->first();

            if ( is_null($employeeFirst) ) {
                return response()->json(['success' => false  ] , 200);
            }
            else if ( $employeeFirst->delete() ) {
                return response()->json(['success' => true  ] , 200);
            }

        } catch ( \Exception $e ) {

        }
    }


    public function generateEmail ( $firstName , $firstSurname , $countryID ) {
        $emailAux = $this->serializeText($firstName).".".$this->serializeText($firstSurname);
        $email = '';
        $countMails = Employee::where("mail","LIKE", "%".$emailAux."%")->count();

        if ( $countMails === 0  )
            $email = $emailAux . (($countryID  == 1 ) ? self::emailBaseCol : self::emailBaseUsa) ;
        else if ( $countMails > 0  )
            $email = $emailAux . "." . ( $countMails ) . (($countryID  == 1 ) ? self::emailBaseCol : self::emailBaseUsa) ;

        return $email;
    }

    public function serializeText ( $text ) {
        return str_replace(' ', '', trim( $text ) );
    }
}
