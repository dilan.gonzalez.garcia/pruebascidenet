@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col text-right">
                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modalAdd" onclick="changeDataID()">
                    Adicionar empleado
                </button>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalAdd" tabindex="-1" aria-labelledby="modalAddLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalAddLabel">Empleado</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="modal-add">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label for="first_name">{{__('Primer Nombre')}}</label>
                            <input type="text" class="form-control" id="first_name" name="first_name"  onkeyup="uppercase(this)" >
                        </div>
                        <div class="form-group">
                            <label for="second_name">{{__('Segundo Nombre')}}</label>
                            <input type="text" class="form-control" id="second_name" name="second_name" onkeyup="uppercase(this)" >
                        </div>
                        <div class="form-group">
                            <label for="surname">{{__('Primer Apellido')}}</label>
                            <input type="text" class="form-control" id="surname" name="surname"  onkeyup="uppercase(this)" >
                        </div>
                        <div class="form-group">
                            <label for="second_surname">{{__('Segundo Apellido')}}</label>
                            <input type="text" class="form-control" id="second_surname" name="second_surname" onkeyup="uppercase(this)" >
                        </div>
                        <div class="form-group">
                            <label for="country_id">{{__('País')}}</label>
                            <select name="country_id" id="country_id" class="form-control">
                                <option value="" disabled>Seleccione</option>
                                @foreach ($country as $value )
                                    <option value="{{$value->id}}">{{$value->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="type_identification_id">{{__('Tipo de indentificación')}}</label>
                            <select name="type_identification_id" id="type_identification_id" class="form-control">
                                <option value="" disabled>Seleccione</option>
                                @foreach ($typeIdentification as $value )
                                    <option value="{{$value->id}}">{{$value->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="identification">{{__('Número de identificación')}}</label>
                            <input type="text" class="form-control" id="identification" name="identification">
                        </div>
                        <div class="form-group">
                            <label for="date_start">{{__('Fecha  ingreso')}}</label>
                            <input type="date" class="form-control" id="date_start" name="date_start" min="{{date( "Y-m-d" , time() - (60*60*24*30) )}}" max="{{date( "Y-m-d" , time() )}}">
                        </div>
                        <div class="form-group">
                            <label for="work_area_id">{{__('Area de trabajp')}}</label>
                            <select name="work_area_id" id="work_area_id" class="form-control">
                                <option value="" disabled>Seleccione</option>
                                @foreach ($workArea as $value )
                                    <option value="{{$value->id}}">{{$value->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary btn-add">Adicionar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row justify-content-center">

            {{ $dataTable->table() }}

        </div>
    </div>
@endsection

<style>
    #first_name , #second_name , #surname , #second_surname {
        text-transform: uppercase;
    }
</style>

@push('scripts')
    {{$dataTable->scripts()}}

    <script>
        document.querySelector('.btn-add').addEventListener('click' , event => {
            event.preventDefault();
            let changeId = document.querySelector("form#modal-add").getAttribute( 'data-change-id' );
            let url = ( changeId === "" ) ? "/employee/create" : "/employee/update/"+changeId ;
            let method = ( changeId === "" ) ? "POST" : "PUT" ;
            fetch(url,
                {"credentials":"include",
                    "headers":
                        {
                            "accept":"*/*",
                            "cache-control":"no-cache",
                            "content-type":"application/x-www-form-urlencoded; charset=UTF-8",
                            "x-requested-with":"XMLHttpRequest"
                        },
                    "body":$("form#modal-add").serialize(),
                    "method":method,
                    "mode":"cors"
                }).then(response => response.json())
                .then(response => {
                    if ( response.success ) {
                        document.querySelector("form#modal-add").reset();
                        Swal.fire({
                            title: 'Exitoso',
                            text: ( ( changeId === "" ) ?  'Registo creado exitosamente.' : 'Registro actualizado exitosamente.' ),
                            icon: 'success',
                            confirmButtonText: 'Cerrar'
                        })
                        window.LaravelDataTables["employee-table"].ajax.reload();
                        $("#modalAdd").modal('hide');
                        document.querySelector("form#modal-add").setAttribute( 'data-change-id' , '' );
                    }
                    else if ( typeof response.errors === "object" ) {
                        var respuesta = ``;
                        $.each(response.errors, function(key, value){
                            respuesta += value+`<br>`;
                        });
                        Swal.fire({
                            title: '¡Error!',
                            html: respuesta,
                            icon: 'error',
                            confirmButtonText: 'Cerrar'
                        })
                    }
                    console.log(response)
                })
                .catch( error => {
                    console.error(error);
                });

        });


        function changeDataID ()
        {
            document.querySelectorAll('label[for="date_start"] , #date_start').forEach( element => element.style.display = 'block' );
            document.querySelector("form#modal-add").setAttribute( 'data-change-id' , '' )
            document.querySelector("form#modal-add").reset();
        }

        function updateEmployee ( id ) {
            let formData = "_token=" + document.querySelector("meta[name='csrf-token']").getAttribute("content")+"&id="+id;

            document.querySelector("form#modal-add").setAttribute( 'data-change-id' , id );
            document.querySelectorAll('label[for="date_start"] , #date_start').forEach( element => element.style.display = 'none' );

            fetch('/employee/update',
                {"credentials":"include",
                    "headers":
                        {
                            "accept":"*/*",
                            "cache-control":"no-cache",
                            "content-type":"application/x-www-form-urlencoded; charset=UTF-8",
                            "x-requested-with":"XMLHttpRequest"
                        },
                    "body":formData,
                    "method":"POST",
                    "mode":"cors"
                }).then(response => response.json())
                .then(response => {
                    if ( response.success ) {
                        $("#modalAdd").modal('show');
                        Object.entries(response.employee).forEach( ( element ) => {
                            if ( element[0] !== "id" && element[0] !== "mail"
                                && element[0] !== "state_id"  && element[0] !== "created_at"
                                && element[0] !== "updated_at" ) {
                                if ( element[0] === "date_start" )
                                    element[1] = element[1].split(" ")[0]
                                document.querySelector(`#${element[0]}`).value = element[1];
                            }
                        })
                    }
                })
                .catch( error => {
                    console.error(error);
                });

        }

        function deleteEmployee ( id ) {
            let formData = "_token=" + document.querySelector("meta[name='csrf-token']").getAttribute("content")+"&id="+id;

            fetch('/employee/delete',
                {"credentials":"include",
                    "headers":
                        {
                            "accept":"*/*",
                            "cache-control":"no-cache",
                            "content-type":"application/x-www-form-urlencoded; charset=UTF-8",
                            "x-requested-with":"XMLHttpRequest"
                        },
                    "body":formData,
                    "method":"DELETE",
                    "mode":"cors"
                }).then(response => response.json())
                .then(response => {
                    if ( response.success ) {
                        Swal.fire({
                            title: 'Exitoso',
                            text: 'Eliminado correctamente',
                            icon: 'success',
                            confirmButtonText: 'Cerrar'
                        })
                        window.LaravelDataTables["employee-table"].ajax.reload();
                    }
                })
                .catch( error => {
                    console.error(error);
                });

        }

        function uppercase (e) {
            e.value = e.value.toUpperCase();
        }

    </script>
@endpush
